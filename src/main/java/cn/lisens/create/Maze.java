package cn.lisens.create;

import java.util.HashMap;
import java.util.Map;

public class Maze {
	Map<Integer,Room> rooms = new HashMap<Integer,Room>();
	
	public Room roomNo(int no) {
		return rooms.get(no);
	}
	public void addRoom(Room room) {
		rooms.put(room.getRoomNumber(),room);
	}
}
