package cn.lisens.create;

import cn.lisens.create.singleton.Singleton;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		//Singleton模式
		Singleton.getInstance();
		System.out.println("Hello World!");
	}
}
