package cn.lisens.create.factorymethod;

public class DogFactory implements AnimalFactory {
	
	@Override
	public Animal createAnimal() {
		Dog dog = new Dog();
		return dog;
	}
}
