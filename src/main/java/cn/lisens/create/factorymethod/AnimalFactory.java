package cn.lisens.create.factorymethod;

public interface AnimalFactory {
	
	public Animal createAnimal();
	
}
