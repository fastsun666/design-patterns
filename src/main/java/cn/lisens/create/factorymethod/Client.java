package cn.lisens.create.factorymethod;

public class Client {
	public static void main(String[] args) {
		AnimalFactory factory = new CatFactory();
		Animal cat = factory.createAnimal();
	}
	
}
