package cn.lisens.create;

public class Room extends MapSite{
	
	private int roomNumber;
	private MapSite[] sides = new MapSite[4];
	
	public Room(int roomNumber) {
		this.roomNumber = roomNumber;
	}
	public void setSide(Direction direction,MapSite mapSite) {
		sides[direction.ordinal()] = mapSite;
	}
	
	@Override
	void enter() {
		// TODO Auto-generated method stub
		
	}

	public int getRoomNumber() {
		return roomNumber;
	}
}
