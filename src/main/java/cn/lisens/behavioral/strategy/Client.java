package cn.lisens.behavioral.strategy;

public class Client {
	public static void main(String[] args) {
		SendMessage sendMessage = new SendMessage();
		sendMessage.register(new AppMessageNotifier());
		sendMessage.register(new SMSMessageNotifier());
		sendMessage.send("hello world!", 2);
	}
}
