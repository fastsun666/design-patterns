package cn.lisens.behavioral.strategy;

public class SMSMessageNotifier implements MessageNotifier {

	@Override
	public boolean support(int notifyType) {
		return notifyType == 1;
	}

	@Override
	public void handle(String info) {
		System.err.println("短信发送："+info);

	}

}
