package cn.lisens.behavioral.strategy;

public class AppMessageNotifier implements MessageNotifier {

	@Override
	public boolean support(int notifyType) {
		return notifyType == 2;
	}

	@Override
	public void handle(String info) {
		System.err.println("应用推送："+info);
		
	}

}
