package cn.lisens.behavioral.strategy;

import java.util.ArrayList;
import java.util.List;

public class SendMessage {
	
	List<MessageNotifier> messageNotifiers = new ArrayList<MessageNotifier>();
	
	public void register(MessageNotifier messageNotifier) {
		messageNotifiers.add(messageNotifier);
	}
	
	public void send(String info, int notifyType) {
		for (MessageNotifier messageNotifier : messageNotifiers) {
			if(messageNotifier.support(notifyType)) {
				messageNotifier.handle(info);
			}
		}
	}
}
