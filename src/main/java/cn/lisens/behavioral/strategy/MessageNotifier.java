package cn.lisens.behavioral.strategy;

public interface MessageNotifier {

	boolean support(int notifyType);
	
	void handle(String info);
}
